0
00:00:00,000 --> 00:00:03,040
all right we now need to learn about

1
00:00:01,199 --> 00:00:05,040
computer registers in general because

2
00:00:03,040 --> 00:00:06,799
you can't understand assembly unless you

3
00:00:05,040 --> 00:00:08,320
understand the computer registers that

4
00:00:06,799 --> 00:00:10,880
assembly languages

5
00:00:08,320 --> 00:00:13,040
operate on so hopefully at some point

6
00:00:10,880 --> 00:00:14,480
you've seen a computer memory hierarchy

7
00:00:13,040 --> 00:00:15,360
but if you haven't I'll just go over

8
00:00:14,480 --> 00:00:17,199
briefly

9
00:00:15,360 --> 00:00:20,160
the basic point is that at the top of

10
00:00:17,199 --> 00:00:23,760
the hierarchy are the contents which are

11
00:00:20,160 --> 00:00:26,480
very small storage and very quick

12
00:00:23,760 --> 00:00:28,560
access and also frequently are volatile

13
00:00:26,480 --> 00:00:30,320
so there will be some volatile memory on

14
00:00:28,560 --> 00:00:30,800
the top and then there will be long-term

15
00:00:30,320 --> 00:00:33,200
memory

16
00:00:30,800 --> 00:00:34,480
and long-term storage which works after

17
00:00:33,200 --> 00:00:36,960
power off

18
00:00:34,480 --> 00:00:37,920
towards the bottom so computer registers

19
00:00:36,960 --> 00:00:40,079
are at the very top

20
00:00:37,920 --> 00:00:41,200
because they are the absolute smallest

21
00:00:40,079 --> 00:00:43,520
amount of memory

22
00:00:41,200 --> 00:00:45,360
frequently on the order of tens of bytes

23
00:00:43,520 --> 00:00:47,600
or hundreds of bytes

24
00:00:45,360 --> 00:00:48,960
and they're very fast to operate on

25
00:00:47,600 --> 00:00:52,079
that's why assembly

26
00:00:48,960 --> 00:00:53,760
accesses them directly but

27
00:00:52,079 --> 00:00:55,600
they're also volatile so when you turn

28
00:00:53,760 --> 00:00:57,360
off the system it's going to lose all

29
00:00:55,600 --> 00:00:58,960
the contents of registers

30
00:00:57,360 --> 00:01:01,280
lower down on the hierarchy you then

31
00:00:58,960 --> 00:01:03,760
have things like the processor cache

32
00:01:01,280 --> 00:01:05,600
which is very fast and which is

33
00:01:03,760 --> 00:01:08,880
expensive to actually you know

34
00:01:05,600 --> 00:01:11,760
generate and put into the processor

35
00:01:08,880 --> 00:01:13,280
and it has small size but it's going to

36
00:01:11,760 --> 00:01:15,200
still lose its

37
00:01:13,280 --> 00:01:16,880
memory contents after you power the

38
00:01:15,200 --> 00:01:17,280
system off the point of the cache of

39
00:01:16,880 --> 00:01:19,119
course

40
00:01:17,280 --> 00:01:20,320
is to make it so that things are faster

41
00:01:19,119 --> 00:01:23,920
than going out to

42
00:01:20,320 --> 00:01:26,400
full random access memory ram so ram

43
00:01:23,920 --> 00:01:27,840
is larger than cache so cache will

44
00:01:26,400 --> 00:01:30,320
typically be on the order of

45
00:01:27,840 --> 00:01:32,000
megabytes for instance whereas ram is on

46
00:01:30,320 --> 00:01:32,720
the order of gigabytes and ram is pretty

47
00:01:32,000 --> 00:01:36,320
fast but

48
00:01:32,720 --> 00:01:37,759
cache is faster and so again all of these

49
00:01:36,320 --> 00:01:39,360
first three things are going to be

50
00:01:37,759 --> 00:01:41,280
volatile memory which you lose the

51
00:01:39,360 --> 00:01:42,399
contents of when you power the system

52
00:01:41,280 --> 00:01:44,399
off

53
00:01:42,399 --> 00:01:45,439
cache is fairly expensive to build into

54
00:01:44,399 --> 00:01:48,960
the processor and

55
00:01:45,439 --> 00:01:51,040
ram is less expensive and larger size

56
00:01:48,960 --> 00:01:52,880
then you get into the non-volatile

57
00:01:51,040 --> 00:01:55,920
storage things like flash or

58
00:01:52,880 --> 00:01:58,640
flash or usb memory which retains its

59
00:01:55,920 --> 00:02:00,479
contents after the system powers off

60
00:01:58,640 --> 00:02:02,240
and that'll be you know typically orders

61
00:02:00,479 --> 00:02:03,600
of you know gigabytes or

62
00:02:02,240 --> 00:02:06,079
eventually you know we're starting to

63
00:02:03,600 --> 00:02:08,640
get terabyte flash

64
00:02:06,079 --> 00:02:09,759
flash storage then you have the

65
00:02:08,640 --> 00:02:11,360
slower cheaper

66
00:02:09,759 --> 00:02:12,879
hard drives which will be you know

67
00:02:11,360 --> 00:02:15,120
multiple terabytes

68
00:02:12,879 --> 00:02:16,640
and then tape drives which can be

69
00:02:15,120 --> 00:02:18,400
getting into the petabytes

70
00:02:16,640 --> 00:02:19,920
so the point we're going to care about

71
00:02:18,400 --> 00:02:21,520
here though is the

72
00:02:19,920 --> 00:02:23,840
registers that are baked into the

73
00:02:21,520 --> 00:02:23,840
computer

